package com.tema3.fragmentssample;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class FragmentoTest extends Fragment {

    private TextView textView;

    @Nullable
    @Override
    // Este método ha de devolver la vista del fragment ya creada. Es habitual y recomendable inicializar
    // aquí otras vistas del layout, como se hace con el TextView.
    // No es recomendable realizar tareas complejas en este método, se está inicializando la vista y debe
    // realizarse lo más rápido posible
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_fragment, container, false);
        textView = view.findViewById(R.id.textView);
        textView.setText("Holaaaaaaaa");
        return view;
    }

    //Este método se llama cuando la vista ya está creada. Si se quiere llevar a cabo tareas más complejas,
    // es el método ideal para hacerlo. Si se quiere tener referencia a alguna vista, siempre se puede declarar
    // para toda la clase, como pasa con el textview.
    // También se puede usar el método findViewById a través del método getView, aunque puede dar nulo
    // si la vista no tiene tal elemento (mostrará un warning)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        textView.setText("Holaa");

        textView = getView().findViewById(R.id.textView);
    }
}
