package com.tema3.fragmentssample;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Se inicializan FragmentManager y FragmentTransaction
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();


//        FragmentTwo fragmentTwo = new FragmentTwo();
//        transaction.add(R.id.fragment1, fragmentTwo, "fragment1");
//        transaction.addToBackStack(null);
        // transaction.commit();


        FragmentoTest fragmentoTest = new FragmentoTest();
        transaction.replace(R.id.fragment1, fragmentoTest);
        //transaction.addToBackStack(null);
        transaction.commit();

        Button addFrag1 = findViewById(R.id.button2);
        addFrag1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                FragmentoTest fragmentoTest = new FragmentoTest();
                transaction.replace(R.id.fragment1, fragmentoTest);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        Button addFrag2 = findViewById(R.id.button3);
        addFrag2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                FragmentTwo fragmentTwo = new FragmentTwo();
                transaction.replace(R.id.fragment1, fragmentTwo, "fragment1");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }
}
